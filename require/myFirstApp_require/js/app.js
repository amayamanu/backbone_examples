require.config({
  paths: {
    jquery: 'lib/jquery.min',
    underscore: 'lib/underscore-min',
    backbone: 'lib/backbone',
    text: 'lib/text',
    common: 'common'
  }
});


require(['views/app', 'common'], function(AppView, common){
  var app_view = new AppView;
});