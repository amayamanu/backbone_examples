define(["product", "credit"], function (products, credit) {

    console.log("Function : purchaseProduct");

    return {
        purchaseProduct: function () {
            var credits = credit.getCredits();
            if (credits > 0) {
                products.reserveProduct();
                return true;
            }
            return false;
        }
    }
});