// Class Todo
var Todo = Backbone.Model.extend({
	defaults: {
		title: '',
		completed: false
	}	
});

// class Todo Collection
var TodosCollection = Backbone.Collection.extend({
	model:Todo,
	url: '/todos'
});

// events -  creating new Views 
// Class View
var View = Backbone.View.extend({
	el: '#todo',

	// bind to DOM event
	events: {
		'click [type="checkbox"]': 'clicked',
	},

	initialize: function(){
		this.$el.click(this.jqueryClicked);
		this.on();
	}
});

var TodoRouter = Backbone.Router.extend({
  /* define the route and function maps for this router */
  routes: {
    "about" : "showAbout",
    "search/:query" : "searchTodos",
    "search/:query/p:page" : "searchTodos",
    "todo/:id": "changeTodo",
    "olaquease/:id": "printTesteando"

  },

  showAbout: function(){},

  searchTodos: function(query, page){
    var page_number = page || 1;
    console.log("Page number: " + page_number + " of the results for todos containing the word: " + query);
  },

  changeTodo: function(id){
  	this.navigate('/olaquease/' + id, {trigger: true})
  },

  printTesteando: function(id){
  	console.log ('testeando o q ase' + id);
  }

});

var myTodoRouter = new TodoRouter();
Backbone.history.start();