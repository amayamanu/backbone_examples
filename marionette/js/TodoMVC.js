var TodoMVC = new Backbone.Marionette.Application();

TodoMVC.addRegions({
  header: '#header',
  main: '#main',
  footer: '#footer'
});

TodoMVC.on('initialize:after', function() {
  Backbone.history.start();
});

  $(function() {
    // Start the TodoMVC app (defined in js/TodoMVC.js)
    TodoMVC.start();
  });